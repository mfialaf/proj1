//
//  FiniteAutomata.swift
//  FiniteAutomata
//
//  Created by Filip Klembara on 17/02/2020.
//
import Foundation

/// Finite automata
public struct FiniteAutomata {
    public let states: [String]
    public let symbols: [String]
    public let transitions: [[String: String]]
    public let initialState: String
    public let finalStates: [String]

//    init(states: [String]? = nil,
//         symbols: [String]? = nil,
//         transitions: [[String: String]] = [[nil: nil]],
//         initialState: String? = nil,
//         finalStates: [String]? = nil) {
//
//        self.states = states
//        self.symbols = symbols
//        self.transitions = transitions
//        self.initialState = initialState
//        self.finalStates = finalStates
//    }

    public func undefined_states_and_symbols_check() throws -> (){

        // Transition symbol & states
        for transition in transitions {
            guard let unwrapFrom = transition["from"] else {
//                print("Transition - from nema state")
                throw AutomataRunError.undefinedSymbol
            }
            if !self.states.contains(unwrapFrom) {
//                print("Transition - retezec from obsahuje nepovoleny state")
                throw AutomataRunError.undefinedState
            }

            guard let unwrapTo = transition["to"] else {
//                print("Transition - to nema state")
                throw AutomataRunError.undefinedState
            }
            if !self.states.contains(unwrapTo) {
//                print("Transition - retezec to obsahuje nepovoleny state")
                throw AutomataRunError.undefinedState
            }
            guard let unwrapWith = transition["with"] else {
 //               print("Transition - with nema symbol")
                throw AutomataRunError.undefinedSymbol
            }
            if !self.symbols.contains(unwrapWith) {
 //               print("Transition - retezec with obsahuje nepovoleny symbol")
                throw AutomataRunError.undefinedSymbol
            }
        }

        // Transition symbol & states
        for transition in transitions {
            guard let unwrapFrom = transition["from"] else {
                print("Transition - from nema state")
                throw AutomataRunError.undefinedSymbol
            }
            if !self.states.contains(unwrapFrom) {
                print("Transition - retezec from obsahuje nepovoleny state")
                throw AutomataRunError.undefinedState
            }

            guard let unwrapTo = transition["to"] else {
                print("Transition - to nema state")
                throw AutomataRunError.undefinedState
            }
            if !self.states.contains(unwrapTo) {
                print("Transition - retezec to obsahuje nepovoleny state")
                throw AutomataRunError.undefinedState
            }
            guard let unwrapWith = transition["with"] else {
                print("Transition - with nema symbol")
                throw AutomataRunError.undefinedSymbol
            }
            if !self.symbols.contains(unwrapWith) {
                print("Transition - retezec with obsahuje nepovoleny symbol")
                throw AutomataRunError.undefinedSymbol
            }
        }

        // Empty final states
        if finalStates.isEmpty {
//            print("prazdno")
            throw AutomataRunError.undefinedState
        }

        // Empty initial state
        if initialState == "" {
//            print("prazdno pocatecni stav")
            throw AutomataRunError.undefinedState
        }

        //Initial state contains state
        if !states.contains(initialState) {
//            print("Pocatecni stav je nepovoleny stav")
            throw AutomataRunError.undefinedState
        }

        //Final states contains state
        for finalState in finalStates {
            if !states.contains(finalState) {
//                print("Koncovy stav je nepovoleny stav")
                throw AutomataRunError.undefinedState
            }
        }
    }
}

public enum AutomataRunError: Error {
    case undefinedState
    case undefinedSymbol
    case notAccepted
}

extension FiniteAutomata: Decodable {
//    var states: [String]
//    public let symbols: [String]?
//    public let transitions: [String]?
//    public let initialState: String?
//    public let finalStates: [String]?

}
