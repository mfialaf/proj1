//
//  Simulator.swift
//  Simulator
//
//  Created by Filip Klembara on 17/02/2020.
//

import FiniteAutomata

/// Simulator
public class Simulator {
    /// Finite automata used in simulations
    private let finiteAutomata: FiniteAutomata
    public var symbolIndex: Int
    public var currentState: String
    public var outputArray: [String]
    public var stack = Stack()
    public var actualSymbol: String
    public var string_array: [String]

    /// Initialize simulator with given automata
    /// - Parameter finiteAutomata: finite automata
    public init(finiteAutomata: FiniteAutomata) {
        self.finiteAutomata = finiteAutomata
        self.symbolIndex = 0
        self.currentState = finiteAutomata.initialState
        self.outputArray = [currentState]
        self.actualSymbol = ""
        self.string_array = []
    }

    private func find_next_transition_ways() -> Bool
    {
        var noWayFound = true
        for transition in finiteAutomata.transitions
        {
            if transition["from"] == self.currentState && transition["with"] == self.actualSymbol
            {
                noWayFound = false
                let pushedTransitionData = data(with: transition["with"]!, to: transition["to"]!, from: transition["from"]!)
                let pushedTransition = stackObject(data: pushedTransitionData, number: self.symbolIndex)
                self.stack.push(pushedTransition)
            }
        }
    return noWayFound
    }

    private func fill_string_array(_ string: String) -> ()
    {
        let input_string_arr = string.components(separatedBy: ",")
        for string in input_string_arr {
            self.string_array.append(string)
        }
    }

    private func inicializace(_ string: String) -> ()
    {
        self.symbolIndex = 0
        self.currentState = finiteAutomata.initialState
        self.outputArray = [currentState]
        stack = Stack()
        self.string_array = []
        fill_string_array(string)
        self.actualSymbol = string_array[self.symbolIndex]
    }
    /// Simulate automata on given string
    /// - Parameter string: string with symbols separated by ','
    /// - Returns: Empty array if given string is not accepted by automata,
    ///     otherwise array of states
//    public func simulate(_ string_array: [String]) -> [String] {
    public func simulate(on string: String) -> [String] {

        inicializace(string)

        // Empty input string
        if string == "" {
            return [self.finiteAutomata.initialState]
        }


        var endlessCyclus = 0
        while true
        {
        endlessCyclus += 1
        if endlessCyclus > 2000 {
 //           print("Nekonecny cyklus")
            break
        }
            if find_next_transition_ways()
            {
                var pom = 0
                while pom < (self.symbolIndex-stack.getNumber())
                {
                    pom += 1
                    if (self.symbolIndex-stack.getNumber()) > self.outputArray.count || (self.symbolIndex-stack.getNumber()) > self.symbolIndex
                    {
//                        print("Index out of range!")
                        break
                    }
                    let _ = self.outputArray.popLast()
                }
                self.symbolIndex = stack.getNumber()
            }

            if stack.peek().number == 666 {
//                print("Chyba pri dekodovani automatu")
                return []
            }

            self.currentState = stack.peek().data.to
            self.outputArray.append(self.currentState)

            //End of automata decoding check
            if finiteAutomata.finalStates.contains(self.currentState) && string_array.count-1 == self.symbolIndex {
                break
            }

            let _ = stack.pop()
            self.symbolIndex += 1
            if !(self.symbolIndex >= string_array.count ) {
                self.actualSymbol = string_array[self.symbolIndex]
            }
        }

    return outputArray
    }

}
