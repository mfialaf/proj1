//
//  Stack.swift
//  Simulator
//
//  Created by Marek Fiala on 31.03.2021.
//

import Foundation

public struct data {
    public var with: String
    public var to: String
    public var from: String
}

public struct stackObject {
    public var data: data
    public var number: Int
}

public struct Stack {
  public var myArray: [stackObject] = []
    
    private let vrchol = stackObject(data: data(with: "peek", to: "peek", from: "peek"), number: 666)
    
    mutating func push(_ element: stackObject){
        myArray.append(element)
    }
    
    mutating func pop() -> stackObject? {
        return myArray.popLast()
    }
    
    func peek() -> stackObject {
        guard let topElement = myArray.last else {return vrchol}
        return topElement
    }
    
    func getNumber () -> Int {
        return peek().number
    }
    
    mutating func changeNumber(_ number: Int) -> () {
        var old = peek()
        old.number = number
        let _ = self.pop()
        push(old)
    }
}
