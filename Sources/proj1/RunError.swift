//
//  RunError.swift
//  proj1
//
//  Created by Filip Klembara on 17/02/2020.
//

enum RunError: Error {
    // *******************
    // * NOT IMPLEMENTED *
    // *******************
    case notImplemented
    case wrongArgument
    case fileReadingError
    case automataDecoding
    case undefinedState
    case undefinedSymbol
    case inputStringIsNotAcceptable
}

// MARK: - Return codes
extension RunError {
    var code: Int {
        switch self {
        case .notImplemented:
            return 66
        case .inputStringIsNotAcceptable:
            return 6
        case .wrongArgument:
            return 11
        case .fileReadingError:
            return 12
        case .automataDecoding:
            return 20
        case .undefinedState:
            return 21
        case .undefinedSymbol:
            return 22
        // *******************
        // * NOT IMPLEMENTED *
        // *******************
        }
    }
}

// MARK:- Description of error
extension RunError: CustomStringConvertible {
    var description: String {
        switch self {
        case .notImplemented:
            return "Not implemented"
        case .wrongArgument:
            return "Wrong argument"
        case .fileReadingError:
            return "Reading from file unsuccesfull"
        case .automataDecoding:
            return "Automata decoding error"
        case .undefinedState:
            return "Automata contains undefined state"
        case .undefinedSymbol:
            return "Automata contains undefined symbol"
        case .inputStringIsNotAcceptable:
            return "Input string is not accepted"
        // *******************
        // * NOT IMPLEMENTED *
        // *******************
        }
    }
}
