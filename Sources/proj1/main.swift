//
//  main.swift
//  proj1
//
//  Created by Filip Klembara on 17/02/2020.
//

import Foundation
import FiniteAutomata
import Simulator

// MARK: - Functions
func ArgumentParser(_ json_data: inout Data!) -> Result<Void, RunError> {
    
    // Number of arguments control.
    if CommandLine.arguments.count != 3 {
        return .failure(.wrongArgument)
    }
    
    // Reading strings of symbols.
//    let first_argument: String = CommandLine.arguments[1]
//    let input_string_arr = first_argument.components(separatedBy: ",")
//
//    for string in input_string_arr {
//        string_array.append(string)
//    }

    // Reading input file.
    let input_json = CommandLine.arguments[2]
    do
    {
        let fileUrl = URL(fileURLWithPath: input_json)
        let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
        json_data = data
    }
    catch {
        return .failure(.fileReadingError)
    }

    return .success(())
}

func AutomataErrorHandling(_ automata: FiniteAutomata) -> Result<Void, RunError> {
    
    do{
        try automata.undefined_states_and_symbols_check()
    } catch AutomataRunError.undefinedState {
        return .failure(.undefinedState)
    }catch AutomataRunError.undefinedSymbol {
        return .failure(.undefinedSymbol)
    } catch {
        
    }
    
    return .success(())
}

// MARK: - Main
func main() -> Result<Void, RunError> {
    
//    var string_array: [String] = []           // Array of automata strings
    var json_data: Data!                            // Json decoded data
    var automata: FiniteAutomata              // Finite automata
    
    // Argument parsing
    let argument_parser_result = ArgumentParser(&json_data)
    if case .failure = argument_parser_result {
        return argument_parser_result
    }
    
    // JSON decoding
    do{
    let decoded_automat = try JSONDecoder().decode(FiniteAutomata.self, from: json_data)
        automata = decoded_automat
    }catch{
        return .failure(.automataDecoding)
    }
    
    // Automata errors handling
    let error_handling = AutomataErrorHandling(automata.self)
    if case .failure = error_handling {
        return error_handling
    }

    // Aoutomata simulation
    let simulator = Simulator(finiteAutomata: automata)
    let output = simulator.simulate(on: CommandLine.arguments[1])
    
    if output == []{
        return .failure(.inputStringIsNotAcceptable)
    }
    for state in output {
        print(state)
    }

    
    return .success(())
}

// MARK: - program body
let result = main()

switch result {
case .success:
    break
case .failure(let error):
    var stderr = STDERRStream()
    print("Error:", error.description, to: &stderr)
    exit(Int32(error.code))
}
